import React from 'react';
import {Alert} from 'react-native';
import LoginForm from './screens/Login.js';
import HomeTabNavigator from './screens/TabNavigatorScreen.js';
import { connect } from 'react-redux';
import { StackNavigator, createStackNavigator } from 'react-navigation';



const AppStackNavigator =  createStackNavigator({
    Login: LoginForm,
    Home: HomeTabNavigator,
});



class MainApp extends React.Component {
    render() {
        return (
            <AppStackNavigator/>
        );
    }
}

const mapStateToProps = (state) => ({
    schedule: state.schedule
});

export default connect(mapStateToProps)(MainApp);
