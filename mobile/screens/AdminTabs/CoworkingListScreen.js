import React, { Component } from 'react';
import {
    View, Text, StyleSheet, ScrollView, TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';


import CoworkingCell from '../StudentTabs/components/CoworkingCell';
import { actionCreators } from '../../actions/allActions.js';



class CoworkingListScreen extends Component {
    static navigationOptions = {
        header: null
    }

    componentDidMount() {
        this.props.fetchAllRooms();
    }

    goToCoworkingScreen(coworking) {
        this.props.navigation.navigate("CoworkingScreen", {coworking});
    }

    goToCoworkingFormScreen() {
        this.props.navigation.navigate("CoworkingFormScreen");
    }

    _renderCoworkingList() {
        let {upi, urgu} = this.props.schedule.halls,
            allRooms = upi.concat(urgu);

        return allRooms.map((room) => {
            let normalName = `${room.corpus.name}-${room.number}`
            return (
                <View style={styles.cell} key={room._id}>
                    <CoworkingCell
                        hall={room}
                        isMy={false}
                        onPress={this.goToCoworkingScreen.bind(this, room)}
                        isEmpty={true} />
                </View>
            )
        })
    }


    render() {
        return (
            <View style={{flex: 1, backgroundColor: "#fff"}}>
                <ScrollView>
                    <View style={styles.container}>
                        <Text style={styles.headerText}>Все коворкинги</Text>
                        <View style={styles.rowCells}>
                            {this._renderCoworkingList.bind(this)()}
                        </View>
                    </View>
                </ScrollView>
                <TouchableOpacity onPress={this.goToCoworkingFormScreen.bind(this)} style={styles.positionNextButton}>
                    <LinearGradient
                        start={[0.0, 1.0]}
                        end={[1.0, 0.0]}
                        colors={["#E35D5D", "#E35D5D"]}
                        style={styles.nextButton}
                    >
                        <MaterialIcons name="add" size={32} color={"#fff"} />
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}


const mapStateToProps = (state) => ({
    schedule: state.schedule,
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllRooms: () => {
            dispatch(actionCreators.fetchAllRooms()).payload.then((response) => {
                !response.error ? dispatch(actionCreators.fetchAllRoomsSuccess(response.data)) : dispatch(actionCreators.fetchAllRoomsError(response.data));
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoworkingListScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 30,
    alignItems: "center"
  },
  headerText: {
    fontSize: 20,
    margin: 10,
    color: "rgba(0,0,0, .5)"
  },
  cell: {
    width: "50%",
  },
  rowCells: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: -10,
    marginRight: -10,
  },
  nextButton: {
    elevation: 2,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    margin: 2,
  },
  positionNextButton: {
    position: "absolute",
    right: 20,
    bottom: 20
  },
});
