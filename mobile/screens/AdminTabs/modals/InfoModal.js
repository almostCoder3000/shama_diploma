import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modal';
import { LinearGradient } from 'expo';



export default class InfoModal extends Component {

    _renderModalInfoContent() {
        let time = socialAccount = phoneNumber = "";

        if ('renter' in this.props.timeLine)
            if (this.props.timeLine.renter !== "") {
                let timeLine = this.props.timeLine;
                socialAccount = timeLine.renter.socialAccount;
                phoneNumber = timeLine.renter.phoneNumber;
                time = `${timeLine.time}, ${timeLine.date}`
            }


        const rows = [
            { name: "Соц.сеть:", value: socialAccount },
            { name: "Номер:", value: phoneNumber },
            { name: "Время:", value: time }
        ]
        return rows.map((row, key) => {
            return (
                <View style={styles.rowInfo} key={key}>
                    <Text>{row.name}</Text>
                    <Text>{row.value}</Text>
                </View>
            )
        })
    }

    _renderModalContent() {
        let firstName = lastName = "";
        if ('renter' in this.props.timeLine)
            if (this.props.timeLine.renter !== "") {
                renter = this.props.timeLine.renter;
                firstName = renter.firstName;
                lastName = renter.lastName;
            }

        return (
            <View style={styles.modalContent}>
                <View style={styles.contentContainer}>
                    <Text style={{color: "rgba(0,0,0,.5)", fontSize: 18, marginBottom: 20}}>{lastName} {firstName}</Text>
                    <View style={{justifyContent: "center", width: "100%"}}>
                        {this._renderModalInfoContent.bind(this)()}
                    </View>
                </View>
                <View style={{flexDirection: "row", width: "100%", height: 50}}>
                    <View style={styles.containerButton}>
                        <TouchableOpacity onPress={this.props.onClose}>
                            <LinearGradient start={[0.0, 1.0]}
                                            end={[1.0, 0.0]}
                                            colors={["#f6d365", "#fda085"]}
                                            style={styles.enrollButton}>
                              <Text style={styles.text}>ОТКЛОНИТЬ</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }


    render() {
        return (
            <Modal
                isVisible={this.props.isVisible}
                onBackdropPress={this.props.onClose}
                onBackButtonPress={this.props.onClose}
                animationIn={'slideInUp'}
                animationOut={'slideOutDown'}
                style={styles.modalStyle}
            >
                {this._renderModalContent.bind(this)()}
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
  text: {
    color: '#fff'
  },
  cancelButton: {
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#F2F3F7",
  },
  containerButton: {
    width: "100%",
    height: "100%",
  },
  enrollButton: {
    alignItems: "center",
    justifyContent: "center",
    height: 50
  },
  contentContainer: {
    padding: 40,
    paddingTop: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  modalStyle: {
    margin: 0,
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%"
  },
  modalContent: {
    backgroundColor: 'white',
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  rowInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
  }
});
