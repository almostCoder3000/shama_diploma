import React from 'react';
import { StyleSheet, TextInput, Text, View, TouchableOpacity, Alert } from 'react-native';

import AwesomeButton from '../based_components/AwesomeButton.js';
import AwesomeTextInput from '../based_components/AwesomeTextInput.js';
import authenticate from '../actions/allActions';
import { connect } from 'react-redux';
import { actionCreators } from '../actions/allActions.js';
import { Bars } from 'react-native-loader';

const config = require('../configure.js');
import {postRequest} from '../helpers.js';



class LoginForm extends React.Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            login: "",
            password: ""
        }
    }

    componentDidMount() {
        /*if (this.props.auth("admin", "1")) {
            this.props.navigation.replace("Home");
        };*/
    }

    submitForm() {
      let {login, password} = this.state;

      if (this.props.auth(login, password)) {
          this.props.navigation.replace("Home");
      };
    }

    render() {
        let {me, loading} = this.props.user;

        if (loading) {
          return (
              <View style={styles.loadingContainer}>
                  <Bars size={16} color="#E35D5D" />
              </View>
          )
        }
        return (
            <View style={styles.container}>
                <View style={styles.form}>
                    <Text style={styles.headerText}>Авторизация</Text>
                    <AwesomeTextInput
                        placeholder='Имя пользователя'
                        value={this.state.login}
                        onChangeText = { (login) => { this.setState({login}) } }/>
                    <AwesomeTextInput
                        placeholder='Пароль'
                        value={this.state.password}
                        onChangeText = { (password) => { this.setState({password}) } }/>
                    <AwesomeButton
                        submitForm={this.submitForm.bind(this)}
                        text='Войти'/>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return {
        auth: (login, password) => {
            return dispatch(actionCreators.auth(login, password)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.authSuccess(res.user)) :
                dispatch(actionCreators.authError(res.error));
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);


const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  container: {
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: "white"
  },
  form: {
    width: '70%',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  headerText: {
    color: "#A2A2A2",
    fontSize: 24,
    marginBottom: 15,
    fontWeight: "100"
  }
});
