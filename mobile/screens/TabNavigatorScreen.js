import React, { Component } from "react";
import {
    View, Text, StyleSheet, Alert
} from "react-native";
import { LinearGradient } from 'expo';

import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { StackActions, NavigationActions } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { Bars } from 'react-native-loader';


import AdminCoworkingListNavigation from './AdminTabs/navigations/CoworkingListNavigation';
import AdminScreenNavigation from './AdminTabs/navigations/AdminScreenNavigation';

import CoworkingListNavigation from './StudentTabs/navigations/CoworkingListNavigation';
import CalendarScreenNavigation from './StudentTabs/navigations/CalendarScreenNavigation';
import Profile from './StudentTabs/Profile';
import { connect } from 'react-redux';



class HomeTabNavigator extends Component {
    static navigationOptions = {
        header: null
    }

    render() {
        let {me, loading} = this.props.user;
        if (loading) {
            return (
                <View style={styles.container}>
                    <Bars size={16} color="#E35D5D" />
                </View>
            )
        }
        if (me.role === "student") {
            return (
                <TabNavigatorScreenStudent screenProps={{ navigation: this.props.navigation }}/>
            )
        } else {
            return (
                <TabNavigatorScreenAdmin screenProps={{ navigation: this.props.navigation }}/>
            )
        }

    }
}


const mapStateToProps = (state) => ({
    user: state.user
});

export default connect(mapStateToProps)(HomeTabNavigator);


const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'CalendarScreen' })],
});


const TabNavigatorScreenStudent = new createMaterialBottomTabNavigator({
    CoworkingList: {
        screen: CoworkingListNavigation,
        navigationOptions: {
            tabBarLabel: "Коворкинги",
            tabBarIcon: ({tintColor}) => (
                <Ionicons name="ios-list" size={28} color={tintColor} style={{top: -5}} />
            ),
            tabBarOnPress: ({defaultHandler, navigation}) => {
                if (navigation.isFocused()) {

                } else {
                    defaultHandler();
                }
            }
        }
    },
    CalendarScreen: {
        screen: CalendarScreenNavigation,
        navigationOptions: {
            tabBarLabel: "Календарь",
            tabBarIcon: ({tintColor}) => (
                <EvilIcons name="calendar" size={28} color={tintColor}/>
            ),
            tabBarOnPress: ({defaultHandler, navigation}) => {
                if (navigation.isFocused()) {
                    //console.log('test::: ', navigation.getChildNavigation());
                    //navigation.dispatch(NavigationActions.navigate({ routeName: 'CalendarScreen' }));
                } else {
                    defaultHandler();
                }
            }
        }
    },
    Profile: {
        screen: Profile,
        navigationOptions: {
            tabBarLabel: "Профиль",
            tabBarIcon: ({tintColor}) => (
                <EvilIcons name="user" size={28} color={tintColor} />
            ),
            tabBarOnPress: ({defaultHandler, navigation}) => {defaultHandler()}
        }
    }
}, {
  activeTintColor: '#fff',
  initialRouteName: 'CoworkingList',
  shifting: 3,
  labeled: false,
  inactiveTintColor: '#F4B9B2',
  barStyle: { backgroundColor: '#E35D5D', height: 50 },
  order: ['CoworkingList', 'CalendarScreen', 'Profile']
});

const TabNavigatorScreenAdmin = new createMaterialBottomTabNavigator({
    CoworkingList: {
        screen: AdminCoworkingListNavigation,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Ionicons name="ios-list" size={28} color={tintColor} style={{top: -5}} />
            ),
            tabBarOnPress: ({defaultHandler, navigation}) => {
                if (navigation.isFocused()) {

                } else {
                    defaultHandler();
                }
            }
        }
    },
    AdminScreen: {
        screen: AdminScreenNavigation,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <EvilIcons name="user" size={28} color={tintColor} />
            ),
            tabBarOnPress: ({defaultHandler, navigation}) => {
                if (navigation.isFocused()) {
                    //console.log('test::: ', navigation.getChildNavigation());
                    //navigation.dispatch(NavigationActions.navigate({ routeName: 'CalendarScreen' }));
                } else {
                    defaultHandler();
                }
            }
        }
    },
}, {
  activeTintColor: '#fff',
  initialRouteName: 'CoworkingList',
  shifting: 3,
  labeled: false,
  inactiveTintColor: '#F4B9B2',
  barStyle: { backgroundColor: '#E35D5D', height: 50 }
});

//for gradient from F59C8C to E35D5D

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
