import React, { Component } from 'react';
import {
  View, Text, StyleSheet, ScrollView, TouchableOpacity, Alert
} from 'react-native';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { LinearGradient } from 'expo';

import EnrollModal from './modals/EnrollModal.js';
import UnenrollModal from './modals/UnenrollModal.js';
import BusyModal from './modals/BusyModal.js';
import AccordionListCoworking from './components/AccordionListCoworking.js';
import { data_calendar } from '../../data/calendar.js';
import { actionCreators } from '../../actions/allActions.js';




class SelectRoomScreen extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        let {date, timeLine, currentTimeIndex} = this.props.navigation.state.params;
        this.state = {
            isOpenEnrollModal: false,
            isOpenBusyModal: false,
            isOpenUnenrollModal: false,
            date: date,
            selectedHall: {},
            currentTime: timeLine[currentTimeIndex],
            currentTimeIndex: currentTimeIndex,
            timeLine: timeLine,
            headerDate: `${date.getDate()} ${data_calendar.months[date.getMonth()]}a`,
        }
    }

    componentDidMount() {
      console.log('work', this.state.currentTimeIndex);
    }


    _goToNextTime() {
        this.props.navigation.push(
            "SelectRoom",
            {
                timeLine: this.state.timeLine,
                date: this.state.date,
                fetchTimeLines: this.props.navigation.state.params.fetchTimeLines,
                currentTimeIndex: this.state.currentTimeIndex + 1
            }
        );
    }

    submitEnrollToCoworking() {
        let date = this.props.navigation.state.params.date;
        let isSuccess = this.props.enrollCoworking(date.toLocaleDateString(), this.state.currentTime.time, this.state.selectedHall);

        if (isSuccess) {
            let id = this.state.selectedHall._id;
            let keys = ['upi', 'urgu'];
            let copyCurrentTime = this.state.currentTime;
            keys.forEach((key) => {
                copyCurrentTime[key].forEach((room, ind) => {
                    console.log(room.room_id, id);
                    if (room.room_id === id) copyCurrentTime[key][ind].renter = {
                        renter: this.props.me
                    }
                });
            });

            this.props.navigation.state.params.fetchTimeLines(date);
            this.props.navigation.pop();
        }

        if (this.state.timeLine.length - 1 !== this.state.currentTimeIndex) {
            this.setState({isOpenEnrollModal: false});
            this._goToNextTime.bind(this)();
        } else {
            this.props.navigation.state.params.fetchTimeLines(date);
            this.props.navigation.popToTop();
        }
    }

    enrollToCoworking(coworking, isMy) {
        this.setState({selectedHall: coworking});
        if (coworking.renter === "") {
            this.setState({isOpenEnrollModal: true});
        } else if (isMy) {
            this.setState({isOpenUnenrollModal: true});
        } else {
            this.setState({isOpenBusyModal: true});
        }
    }

    submitUnenrollToCoworking() {
        let date = this.props.navigation.state.params.date;
        this.props.unenrollCoworking(this.state.selectedHall.renter._id);
        this.props.navigation.pop();
        if (this.state.timeLine.length - 1 !== this.state.currentTimeIndex) {
            this.setState({isOpenUnenrollModal: false});
            this._goToNextTime.bind(this)();
        } else {
            this.props.navigation.state.params.fetchTimeLines(date);
            this.props.navigation.popToTop();
        }
    }

    _extendsHalls(halls) {
        let allHalls = this.props.schedule.allRooms;
        return halls.map((hall) => {
            return {
                ...hall,
                ...allHalls[hall.room_id]
            }
        });
    }

    _onModalShow() {
        return this.state.selectedHall;
    }


    render() {

        let {upi, urgu} = this.state.currentTime;
        let count = upi.length + urgu.length;
        upi = this._extendsHalls(upi);
        urgu = this._extendsHalls(urgu);
        let isVisibleNextButton = this.state.timeLine.length - 1 !== this.state.currentTimeIndex;

        return (
            <View style={{flex: 1}}>
                <ScrollView style={styles.container}>
                    <BusyModal
                        isVisible={ this.state.isOpenBusyModal }
                        hall={this.state.selectedHall}
                        onClose={ () => { this.setState({ isOpenBusyModal: false }) } } />
                    <EnrollModal
                        isVisible={this.state.isOpenEnrollModal}
                        onEnroll={this.submitEnrollToCoworking.bind(this)}
                        hall={this.state.selectedHall}
                        onClose={() => {this.setState({isOpenEnrollModal: false})}} />
                    <UnenrollModal
                        isVisible={this.state.isOpenUnenrollModal}
                        hall={this.state.selectedHall}
                        onUnenroll={this.submitUnenrollToCoworking.bind(this)}
                        onClose={() => {this.setState({isOpenUnenrollModal: false})}} />
                    <View style={styles.headerContainer}>
                        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={styles.backButton}>
                            <Ionicons name="ios-arrow-round-back" size={32} color={"black"} />
                        </TouchableOpacity>
                        <Text style={{fontSize: 18}}>{this.state.headerDate} {this.state.currentTime.time}</Text>
                        <Text style={{color: "#A2A2A2", fontSize: 16}}>{count} всего</Text>
                    </View>
                    <View style={styles.accordionContainer}>
                        <AccordionListCoworking
                            upi={upi}
                            urgu={urgu}
                            me={this.props.me}
                            handleTouchOnCell={this.enrollToCoworking.bind(this)}
                        />
                    </View>

                </ScrollView>
                <TouchableOpacity onPress={this._goToNextTime.bind(this)} style={styles.positionNextButton}>
                    <LinearGradient
                        start={[0.0, 1.0]}
                        end={[1.0, 0.0]}
                        colors={["#f6d365", "#fda085"]}
                        style={[styles.nextButton, {display: isVisibleNextButton ? "flex" : "none"}]}
                    >
                        <MaterialIcons name="navigate-next" size={32} color={"#fff"} />
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    schedule: state.schedule,
    me: state.user.me
});

const mapDispatchToProps = (dispatch) => {
    return {
        enrollCoworking: (date, time, coworking) => {
            console.log('enroll coworking');
            dispatch(actionCreators.enrollCoworking(date, time, coworking)).payload.then((res) => {
                if ("success" in res) {
                    Alert.alert("Вы успешно записаны!");
                    return true;
                } else {
                    console.log('err ', res.error);
                    Alert.alert("Ошибка!");
                    return false;
                }
            })
        },
        unenrollCoworking: (schedule_id) => {
            return dispatch(actionCreators.unenrollCoworking(schedule_id)).payload.then((res) => {
                console.log('res::: ', res);
                return "success" in res
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectRoomScreen)



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  headerContainer: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 30
  },
  backButton: {
    height: 50,
    width: 50,
    position: "absolute",
    top: 40,
    left: 20
  },
  accordionContainer: {
    paddingLeft: 20,
    paddingRight: 20
  },
  nextButton: {
    elevation: 2,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    margin: 2,
  },
  positionNextButton: {
    position: "absolute",
    right: 20,
    bottom: 20
  }

});
