/* @flow */

import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity
} from 'react-native';

export default class Day extends Component {
  constructor(props) {
      super(props);
      this.state = {
          selected: false,
      }
  }

  select() {
      if (this.props.enabled) this.props.onPress();
  }

  _isAccess() {
      return this.props.enabled ? styles.enabledDay : styles.disabledDay;
  }

  render() {
      const styleDay = [this.props.selected && this.props.enabled ? styles.selected : this._isAccess.bind(this)(), styles.textDayStyle];

      return (
          <TouchableOpacity style={ styles.dayStyle } onPress={this.select.bind(this)}>
              <Text style={ styleDay }>{ this.props.date.getDate() }</Text>
          </TouchableOpacity>
      );
  }
}

const styles = StyleSheet.create({
  textDayStyle: {
    height: 24,
    width: 24,
    borderRadius: 12,
    lineHeight: 22,
    textAlign: "center"
  },
  enabledDay: {
    color: "#fff",
  },
  disabledDay: {
    color: "rgba(255, 255, 255, .4)"
  },
  dayStyle: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  selected: {
    color: "#faba75",
    backgroundColor: "#fff",
  }
});
