import React, { Component } from 'react';
import {
  View, Text, StyleSheet,
} from 'react-native';


import Accordion from 'react-native-collapsible/Accordion';
import CoworkingCell from './CoworkingCell';



export default class AccordionListCoworking extends Component {
    constructor(props) {
        super(props);
        let {upi, urgu} = this.props;

        this.state = {
            sections: [
              {
                title: "УПИ",
                content: () => {
                    return this._renderHalls(upi);
                }
              },
              {
                title: "УрГУ",
                content: () => {
                  return this._renderHalls(urgu);
                }
              }
            ]
        }
    }


    _renderHalls(halls) {
        const jsxHalls = halls.map((hall) => {
            let isEmpty = 'renter' in hall ? hall.renter === "" : true;
            let isMy = this.props.me && 'renter' in hall ? hall.renter !== "" ? hall.renter.renter._id === this.props.me._id : false : false;
            return (
                <View style={styles.cell} key={hall._id}>
                    <CoworkingCell
                        hall={hall}
                        isMy={isMy}
                        navigation={this.props.navigation}
                        onPress={this.props.handleTouchOnCell}
                        isEmpty={isEmpty} />
                </View>
            )
        })
        return (
          <View style={styles.rowCell}>
            {jsxHalls}
          </View>
        )
    }

    _renderHeader(section, _, isActive) {
        let styleHeader = {
            alignItems: 'center',
            paddingTop: 10,
            paddingBottom: 5,
            borderColor: "lightgray"
        };
        styleHeader.borderBottomWidth = isActive? 0 : 1;
        return (
            <View style={styleHeader}>
                <Text>{section.title}</Text>
            </View>
        );
    }

    _renderContent(section) {
        return (
            <View>
                {section.content()}
            </View>
        );
    }


    render() {
        return (
            <View style={styles.mainContainer}>
                <Accordion
                  sections={this.state.sections}
                  renderHeader={this._renderHeader}
                  renderContent={this._renderContent}
                  underlayColor="white"
                  initiallyActiveSection={0}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: "white",
    flex: 1
  },
  headerAccordion: {
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 5,
    borderColor: "lightgray"
  },
  cell: {
    width: "50%",
  },
  rowCell: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: -10,
    marginRight: -10,
  }
});
