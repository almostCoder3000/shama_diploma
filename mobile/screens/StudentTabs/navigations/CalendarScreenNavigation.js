import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Alert, ScrollView, TouchableOpacity, CheckBox, Easing,
  Animated
} from 'react-native';
import { StackNavigator, createStackNavigator } from 'react-navigation';

import SelectRoomScreen from '../SelectRoomScreen.js';
import CalendarScreen from '../CalendarScreen.js';


const CalendarStackNavigation = createStackNavigator({
    Calendar: {
        screen: CalendarScreen
    },
    SelectRoom: {
        screen: SelectRoomScreen
    }
}, {
  transitionConfig: () => ({
    transitionSpec: {
      duration: 300,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;
      const { index } = scene;

      const height = layout.initHeight;
      const translateX = position.interpolate({
        inputRange: [index - 1, index, index + 1],
        outputRange: [height, 0, 0],
      });

      const opacity = position.interpolate({
        inputRange: [index - 1, index - 0.99, index],
        outputRange: [0, 1, 1],
      });

      return { opacity, transform: [{ translateX }] };
    },
  }),
});

export default class CalendarNavigation extends Component {
    render() {
        return <CalendarStackNavigation screenProps={{ navigation: this.props.navigation }} />
    }
}
